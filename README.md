DiQuick Web前端框架，采用 HTML+CSS3+jQuery，配置有响应式布局及预定义设置，包含诸多组件：Nav、Tab、Media、Form、Table、Menu等，致力于快速开发轻量化、语义化、扩展性强的Web前端项目。 

软件中文首页
[http://www.diquick.com/](http://www.diquick.com/)

![#DiQuick
](http://git.oschina.net/uploads/images/2016/1026/111512_8c14b25c_1073834.jpeg "DiQuick")